class Exception {
    constructor(message, context) {
        this.message = message
        this.context = context
    }
}

export class NoMovesRemainingException extends Exception {}