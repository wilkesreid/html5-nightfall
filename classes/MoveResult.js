export default class MoveResult {
    constructor({ distanceMoved, movesRemaining, originalPosition, newPosition }) {
        this.distanceMoved = distanceMoved
        this.movesRemaining = movesRemaining
        this.originalPosition = originalPosition,
        this.newPosition = newPosition
    }
}