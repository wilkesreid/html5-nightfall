const app = require('express')()
const http = require('http').createServer(app)
const io = require('socket.io')(http)
const readline = require('readline')
const nanoid = require('nanoid')
const jwt = require('jsonwebtoken')
const keys = require('./keys')

const DISCONNECTION_TIMEOUT = 5000

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

var connections = 0
var rooms = {
    rooms: [],
    add(name, hostPlayerId) {
        let newRoom = {
            name,
            hostPlayerId,
            gameState: 'lobby',
            players: [{
                playerId: hostPlayerId,
                programs: []
            }]
        }
        this.rooms.push(newRoom)
        return newRoom
    },
    list() {
        return this.rooms.map(room => {
            return {
                name: room.name,
                state: room.gameState,
                numPlayers: room.players.length
            }
        })
    },

    getByName(name) {
        return this.rooms.find(room => room.name == name)
    },
    getByHostId(id) {
        return this.rooms.find(room => room.hostPlayerId == id)
    },
    getByPlayerId(id) {
        return this.rooms.find(room => {
            return room.players.find(player => player.playerId == id)
        })
    },

    // For internal use mainly
    getIndexByName(name) {
        return this.rooms.findIndex(room => room.name == name)
    },
    getIndexByPlayerId(id) {
        return this.rooms.findIndex(room => {
            return room.players.find(player => player.playerId == id)
        })
    },

    removeByName(name) {
        this.rooms = this.rooms.filter(room => room.name != name)
    },
    removeByHostId(id) {
        this.rooms = this.rooms.filter(room => room.hostPlayerId != id)
    },
    removePlayerById(id) {
        let roomIndex = this.getIndexByPlayerId(id)
        this.rooms[roomIndex].players = this.rooms[roomIndex].players.filter(player => player.playerId != id)
    },

    addPlayer(roomName, playerId) {
        let room = this.getByName(roomName)
        if (!room.players.find(player => player.playerId == playerId)) {
            room.players = [...room.players, {
                playerId,
                programs: []
            }]
        }
    }
}
var waitingToDisconnect = {}

io.on('connection', socket => {
    console.log('new connection')

    let playerId = null
    let playerState = {
        hostingRoomName: null,
        joinedRoomName: null,
    }

    socket.on('disconnect', () => {
        console.log('player disconnected')

        /**
         * Do nothing else if the connected user never got an id
         */
        if (playerId === null) {
            return
        }

        /**
         * If the player was hosting a room, start the timer to allow
         * for a reconnect attempt before removing the room
         */
        if (playerState.hostingRoomName) {
            console.log('player was hosting a room. setting up cleanup timeout')
            waitingToDisconnect[playerId] = setTimeout(() => {
                console.log(`cleaning up room hosted by ${playerId}`)
                rooms.removeByHostId(playerId)
                socket.broadcast.emit('roomList', rooms.list())
                socket.broadcast.to(playerState.hostingRoomName).emit('roomClosed')
                socket.leave(playerState.hostingRoomName)
                playerState.hostingRoomName = null
                delete waitingToDisconnect[playerId]
            }, DISCONNECTION_TIMEOUT)
        }

        /**
         * If the player had joined a room, start the timer to allow
         * for a reconnect attempt before removing the player
         */
        else if (playerState.joinedRoomName) {
            console.log('player had joined a room. setting up cleanup timeout')
            waitingToDisconnect[playerId] = setTimeout(() => {
                console.log(`removing player ${playerId} from room ${playerState.joinedRoomName}`)
                rooms.removePlayerById(playerId)
                io.to(playerState.joinedRoomName).emit('roomData', rooms.getByName(playerState.joinedRoomName))
                socket.leave(playerState.joinedRoomName)
                playerState.joinedRoomName = null
                delete waitingToDisconnect[playerId]
            }, DISCONNECTION_TIMEOUT)
        }
    })

    /**
     * Connection registration
     */

    socket.on('requestNewId', () => {
        let id = nanoid()
        console.log('client requested a new id. sending back ' + id)
        let token = jwt.sign({ id }, keys.key)
        socket.emit('newIdRegistered', token)
        playerId = id
    })

    socket.on('presentOwnId', token => {
        console.log('client presented its own id in a jwt. validating it...')
        jwt.verify(token, keys.key, (err, decoded) => {
            if (err) {
                console.log('jwt could not be verified')
                socket.emit('ownIdRejected')
            } else {
                console.log('jwt verified with id ' + decoded.id)
                socket.emit('ownIdAccepted')
                playerId = decoded.id

                /**
                 * If we were going to clear out the room this
                 * player was hosting, prevent clearing out
                 * the room.
                 */
                if (waitingToDisconnect[playerId]) {
                    console.log(`player reconnected, so don't cleanup`)
                    clearTimeout(waitingToDisconnect[playerId])
                }

                /**
                 * Check if player was already hosting a room or
                 * had already joined a room
                 */
                let hostingRoom = rooms.getByHostId(playerId)
                let joinedRoom = rooms.getByPlayerId(playerId)
                if (hostingRoom) {
                    console.log(`informing player that they are hosting room ${hostingRoom.name}`)
                    playerState.hostingRoomName = hostingRoom.name
                    socket.join(hostingRoom.name)
                    socket.emit('hostingRoom', hostingRoom.name)
                    socket.emit('roomData', hostingRoom)
                } else if (joinedRoom) {
                    console.log(`informing player that they already joined room ${joinedRoom.name}`)
                    playerState.joinedRoomName = joinedRoom.name
                    socket.join(joinedRoom.name)
                    socket.emit('joinedRoom', joinedRoom.name)
                    socket.emit('roomData', joinedRoom)
                }
            }
        })
    })

    /**
     * Room management
     */
    socket.on('createRoom', (name, resp) => {
        console.log(`player ${playerId} wants to create a new room called ${name}`)

        // If the room name is taken
        if (rooms.getByName(name)) {
            console.log(`the room name ${name} already exists`)
            resp('roomTaken')
        }
        
        // If the player doesn't have an id (for some reason)
        else if (playerId === null) {
            console.log(`the player does not have a player id`)
            resp('noPlayerId')
        }
        
        // Create the room
        else {
            console.log(`creating room ${name}`)
            let room = rooms.add(name, playerId)
            playerState.hostingRoomName = name
            socket.join(name)
            
            resp(room)
            console.log('informing all clients of room creation')
            socket.broadcast.emit('roomList', rooms.list())
            io.to(name).emit('roomData', room)
        }
    })

    socket.on('getRoomList', () => {
        socket.emit('roomList', rooms.list())
    })

    socket.on('stopHosting', resp => {
        rooms.removeByHostId(playerId)
        socket.broadcast.emit('roomList', rooms.list())
        socket.broadcast.to(playerState.hostingRoomName).emit('roomClosed')

        socket.leave(playerState.hostingRoomName)
        playerState.hostingRoomName = null
        
        resp()
    })

    socket.on('joinRoom', (roomName, resp) => {
        let room = rooms.getByName(roomName)
        console.log(`player ${playerId} wants to join room ${roomName}`)
        playerState.joinedRoomName = roomName

        // Do not let player join if game has already started
        if (room.gameState == 'playing') {
            console.log(`game ${roomName} has already started`)
            return resp('game has already started')
        }

        console.log(`joining player to room ${roomName}`)
        rooms.addPlayer(roomName, playerId)
        socket.join(roomName)

        io.to(roomName).emit('roomData', room)

        resp(room)
    })

    socket.on('startGame', () => {
        let room = rooms.getByHostId(playerId)
        if (!room) {
            return
        }

        room.gameState = 'playing'
        io.to(room.name).emit('roomData', room)
        socket.broadcast.emit('roomList', rooms.list())
    })

    socket.on('leaveRoom', (resp) => {
        let room = rooms.getByPlayerId(playerId)

        /**
         * It's possible we're trying to leave a room that has 
         * already been removed
         */
        if (room) {
            rooms.removePlayerById(playerId)
            io.to(room.name).emit('roomData', room)
        }

        socket.leave(playerState.joinedRoomName)
        playerState.joinedRoomName = null
        resp()
    })

})

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
})

rl.on('line', line => {
    let parts = line.split(' ')
    
    switch (parts[0]) {
        case 'rooms':
            console.log(rooms.list())
            break
        case 'room':
            console.log(rooms.getByName(parts[1]))
            break
        case 'players':
            console.log(rooms.getByName(parts[1]).players)
            break
    }
})

http.listen(3000, () => {
    console.log('listening on port 3000')
})
