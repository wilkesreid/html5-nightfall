import nanoid from 'nanoid'

var server = {
    
    players: [],
    currentPlayer: null,
    events: {},

    registerPlayer(name) {
        var newPlayer = {
            id: nanoid(),
            name,
            programs: []
        }
        this.players.push(newPlayer)
        this.do('update', {
            message: 'newPlayer',
            data: newPlayer
        })
        return newPlayer
    },

    registerProgram(config) {
        var playerIndex = this.players.findIndex(player => player.id == config.owner)
        var program = {
            id: nanoid(),
            ...config
        }
        this.players[playerIndex].programs.push(program)
        this.do('update', {
            message: 'newProgram',
            data: program
        })
    },

    start() {
        this.currentPlayer = this.players[0]
    },

    endTurn() {
        var index = this.players.findIndex(player => player.id == this.currentPlayer.id)
        var nextIndex = (index + 1) % this.players.length
        this.currentPlayer = this.players[nextIndex]
        return this.currentPlayer
    },

    on(event, callback) {
        if (this.events.hasOwnProperty(event)) {
            this.events.push(callback)
        } else {
            this.events[event] = [callback]
        }
    },
    
    do(event, payload) {
        if (this.events.hasOwnProperty(event)) {
            this.events[event].forEach(callback => {
                callback(payload)
            })
        }
    }
}

window.server = server

export default server