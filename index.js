import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import components from './components/all'
import store from './store'

Object.keys(components).forEach(key => {
    Vue.component(key, components[key])
})

new Vue({
    store,
    render: h => h(App)
}).$mount('#app')