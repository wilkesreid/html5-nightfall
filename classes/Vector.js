class Vector {
    constructor(x, y) {
        this.x = x
        this.y = y
    }

    set(vector) {
        this.x = vector.x
        this.y = vector.y
    }

    equals(vector) {
        return this.x == vector.x && this.y == vector.y
    }

    doesNotEqual(vector) {
        return this.x != vector.x || this.y != vector.y
    }

    add(vector) {
        return new Vector(
            this.x + vector.x,
            this.y + vector.y
        )
    }

    sub(vector){
        return new Vector(
            this.x - vector.x,
            this.y - vector.y
        )
    }

    subFrom(vector) {
        return new Vector(
            vector.x - this.x,
            vector.y - this.y
        )
    }

    minmax(min, max) {
        return new Vector(
            Math.min(Math.max(this.x, min.x), max.x),
            Math.min(Math.max(this.y, min.y), max.y)
        )
    }

    /**
     * Please note that THIS IS NOT A REAL DISTANCE FORMULA!
     * This method is meant to calculate how many moves it
     * would take to go from one grid coordinate to another.
     * For example, the real distance on a cartesian graph
     * between 1,1 and 5,5 is 5.65685424, but this method
     * will return 8 because it would take 8 moves to get
     * there.
     */
    distanceTo(vector) {
        return Math.abs(this.x - vector.x) + Math.abs(this.y - vector.y)
    }
}

export default Vector