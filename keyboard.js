import Mousetrap from 'mousetrap'

export function createKeyboardHandlers(store) {
    Mousetrap.reset();

    if (store.state.activeProgram !== null) {
        Mousetrap.bind('up', () => {
            store.state.activeProgram.moveDirection('up')
        })
        Mousetrap.bind('down', () => {
            store.state.activeProgram.moveDirection('down')
        })
        Mousetrap.bind('left', () => {
            store.state.activeProgram.moveDirection('left')
        })
        Mousetrap.bind('right', () => {
            store.state.activeProgram.moveDirection('right')
        })
    }
}