import Vue from 'vue'
import Vuex from 'vuex'
import io from 'socket.io-client'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        activeProgram: null,
        activePlayer: null,
        playerId: null,
        page: 'street', // 'street' or 'lobby' or 'game'
        networkState: null, // null or 'connected'
        wasEverConnected: false, // this is true if we get connected even once
        joinState: null, // null or 'hosting' or 'joined'
        roomData: null,
        players: [],
        roomList: []
    },
    mutations: {
        goToPage(state, page) {
            state.page = page
        },
        activateProgram(state, program) {
            state.activeProgram = program 
        },
        activatePlayer(state, playerId) {
            state.activePlayer = playerId
        },
        addNewProgram(state, program) {
            var playerIndex = state.players.findIndex(player => player.id == program.owner)
            var player = {...state.players[playerIndex]}
            player.programs = [...player.programs, program]
            Vue.set(state.players, playerIndex, player)
        },

        /**
         * Common setters
         */
        setNetworkState: setter('networkState'),
        setJoinState: setter('joinState'),
        setPlayerId: setter('playerId'),
        setRoomData: setter('roomData'),
        setActiveProgram: setter('activeProgram'),
        setRoomList: setter('roomList'),
        setWasEverConnected: setter('wasEverConnected'),

    },
    getters: {
        isMyTurn(state) {
            return state.activePlayer == state.playerId
        },
        amHost(state) {
            return state.roomData.hostPlayerId == state.playerId
        }
    },
    actions: {
        hostRoom({commit, dispatch}, roomData) {
            commit('setRoomData', roomData)
            commit('goToPage', 'lobby')
            commit('setJoinState', 'hosting')
        },
        joinRoom({commit, dispatch}, roomData) {
            commit('setRoomData', roomData)
            commit('goToPage', 'lobby')
            commit('setJoinState', 'joined')
        },
        leaveRoom({commit}) {
            commit('goToPage', 'street')
            commit('setJoinState', null)
            commit('setRoomData', null)
        },
    }
})

/**
 * This utility function makes it easy to craft simple setters
 */
function setter(prop) {
    return function (state, payload) {
        state[prop] = payload
    }
}