import socket from './socket'
import jwt from 'jsonwebtoken'
import store from './store'
import _once from 'lodash/once'
import { when } from 'q'

/**
 * Connection
 */
socket.on('disconnect', () => {
    store.commit('setNetworkState', null)
    store.commit('setPlayerId', null)
})
socket.on('connect', () => {
    registerConnection()
    store.commit('setNetworkState', 'connected')
    store.commit('setWasEverConnected', true)
    getRooms()
})

/**
 * Utilities
 */
function whenReceived(message, callback) {
    socket.on(message, data => {
        callback({message, data})
    })
}

/**
 * Registration
 */
socket.on('ownIdAccepted', () => {
    console.log('the server accepted our id')
    store.commit('setPlayerId', jwt.decode(window.localStorage.token).id)
})

socket.on('ownIdRejected', () => {
    console.log('our id was rejected. clearing token and asking for a new id')
    window.localStorage.token = null
    socket.emit('requestNewId')
})

socket.on('newIdRegistered', token => {
    let decoded = jwt.decode(token)
    console.log('the server assigned us a new id:', decoded.id)
    window.localStorage.token = token
    store.commit('setPlayerId', decoded.id)
})

socket.on('noPlayerId', token => {
    console.log('The server says we have no player id. Asking for a new one.')
    socket.emit('requestNewId')
})

socket.on('hostingRoom', roomName => {
    store.dispatch('hostRoom', roomName)
})

socket.on('joinedRoom', roomName => {
    store.dispatch('joinRoom', roomName)
})

export function registerConnection() {
    if (window.localStorage.token) {
        console.log('presenting our own id', jwt.decode(window.localStorage.token).id)
        socket.emit('presentOwnId', window.localStorage.token)
    } else {
        console.log('asking server for new id')
        socket.emit('requestNewId')
    }
}

export function registerPlayer(name) {
    return new Promise((resolve, reject) => {
        socket.on('playerAccepted', () => {
            console.log('we have been accepted as a player')
        })

        if (window.localStorage.playerName) {
            socket.emit('newPlayer', name)
        }
    })
}

export function registerProgram(config) {
    socket.emit('newProgram', JSON.stringify(config))
}

/**
 * Street Page
 */

whenReceived('roomList', ({data}) => {
    store.commit('setRoomList', data)
})

export function createRoom(name) {
    return new Promise((resolve, reject) => {
        socket.emit('createRoom', name, response => {
            if (typeof response == 'object') {
                resolve(response)
            } else {
                switch(response) {
                    case 'roomTaken':
                        reject(response)
                        break
                    case 'alreadyHosting':
                        reject(response)
                        break
                }
            }
        })
    })
}

export function getRooms(timeout = 5000) {
    return new Promise((resolve, reject) => {
        _once(whenReceived)('roomList', resolve)

        setTimeout(() => {
            reject()
        }, timeout)

        socket.emit('getRoomList')
    })
}

export function joinRoom(roomName) {
    socket.emit('joinRoom', roomName, resp => {
        if (typeof resp == 'object') {
            store.dispatch('joinRoom', resp)
        } else {
            console.error(resp)
        }
    })
}

/**
 * Lobby Page
 */

export function stopHosting() {
    socket.emit('stopHosting', () => {
        store.commit('setJoinState', null)
        store.commit('goToPage', 'street')
    })
}

export function leaveRoom() {
    socket.emit('leaveRoom', () => {
        store.dispatch('leaveRoom')
    })
}

export function startGame() {
    socket.emit('startGame')
}

whenReceived('roomData', ({data}) => {
    store.commit('setRoomData', data)

    if (data.gameState == 'playing') {
        store.commit('goToPage', 'game')
    } else {
        store.commit('goToPage', 'lobby')
    }
})

whenReceived('roomClosed', () => {
    leaveRoom()
})