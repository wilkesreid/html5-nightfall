## HTML5 Nightfall Incident Remake

# Setup

To get setup, you'll need to have the following installed:

- Node
- Yarn
- The Parcel bundler (install by running `yarn global add parcel-bundler`)

Do the following:

1. `git clone https://bitbucket.org/wilkesreid/html5-nightfall`
2. `cd html5-nightfall`
3. `yarn`
4. `cd server`
5. `yarn`

There are two parts: the client, and the server.

To run the client, just run `yarn start` in the main folder. If you're on windows, you will need to directly run `parcel index.html`.

To run the server, run `cd server` and then `yarn start`. If you're on windows, directly run `node server.js`.